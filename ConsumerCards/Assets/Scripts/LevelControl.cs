﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelControl : MonoBehaviour
{
    // Start is called before the first frame update
	public int scene;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
	
	public void SceneLoad()
    {
		SceneManager.LoadScene(scene);
    }
	
	/*public void OnTriggerEnter2D(Collider2D other)
	{
		if(other.CompareTag("Player"))
		{
			SceneManager.LoadScene(1);
		}
	}*/
}
