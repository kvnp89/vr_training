﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//sing System.Threading;
using UnityEngine.SceneManagement;

public class PanelAppear : MonoBehaviour
{
	public GameObject panel;
	public int scene;
	public GameObject audio;
    // Start is called before the first frame update
    void Start()
    {
        //audio.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
	
	public void OnHoverPanelAppear()
	{
		panel.SetActive(true);
		print("Hello world");
	}
	public void OnPointerExitPanelDisAppear()
	{
		panel.SetActive(false);
		print("Bye world");
	}
	
	
	public void SceneLoad()
	{
		SceneManager.LoadScene(scene);
		

	}

	public void OnClcikVoice()
	{
		//audio.Play(audio.clip);
		audio.SetActive(true);
	}
}
